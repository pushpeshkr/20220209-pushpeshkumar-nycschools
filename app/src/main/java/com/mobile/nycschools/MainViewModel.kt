package com.mobile.nycschools

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobile.nycschools.api.WebServiceRepository
import com.mobile.nycschools.data.SchoolsData
import com.mobile.nycschools.data.SchoolsSAT
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class MainViewModel : ViewModel() {

    // Could have used constructor injection
    // But using field inject to avoid boilerplate code for viewmodel factory

    // shared viewmodel - as data is being shared by both the fragements screen
    // like information screen shows data already available from schools list screen service

    @Inject
    lateinit var repository: WebServiceRepository

    @Inject
    lateinit var couroutineDispatcher:CoroutineDispatcher

    var selectedSchoolPosition = 0
    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> by lazy {
        _isLoading
    }

    private var _schoolData = MutableLiveData<List<SchoolsData>>()
    val schoolData : LiveData<List<SchoolsData>> by lazy {
        _schoolData
    }

    private var _schoolSAT = MutableLiveData<List<SchoolsSAT>>()
    val schoolSAT : LiveData<List<SchoolsSAT>> by lazy {
        _schoolSAT
    }

    private val _errorMessageSchoolData = MutableLiveData<String?>(null)
    val errorMessageSchoolData: LiveData<String?> by lazy{
        _errorMessageSchoolData
    }

    private val _errorMessageSchoolSAT = MutableLiveData<String?>(null)
    val errorMessageSchoolSAT: LiveData<String?> by lazy{
        _errorMessageSchoolSAT
    }

    // Could move below network call to repository

    suspend fun fetchSchoolData() {
        viewModelScope.launch(couroutineDispatcher) {
            _errorMessageSchoolData.postValue(null)
            _isLoading.postValue(true)
            try {
                val fetchedSchoolData = repository.retrofitService().getSchoolsData()
                _schoolData.postValue(fetchedSchoolData)
            } catch (e: Exception) {
                _errorMessageSchoolData.postValue(ERROR_MSG)
            } finally {
                _isLoading.postValue(false)
            }
        }
    }

    suspend fun fetchSchoolSAT(){
        viewModelScope.launch(couroutineDispatcher) {
            _errorMessageSchoolSAT.postValue(null)
            _isLoading.postValue(true)
            try {
                val fetchedSchoolSAT = repository.retrofitService()
                    .getSchoolsSAT(getSchoolId(selectedSchoolPosition))
                _schoolSAT.postValue(fetchedSchoolSAT)
            } catch (e: Exception) {
                _errorMessageSchoolSAT.postValue(ERROR_MSG)
            } finally {
                _isLoading.postValue(false)
            }
        }
    }

    private fun getSchoolId(position:Int) : String{
        return _schoolData.value?.get(position)?.dbn ?: ""
    }
}