package com.mobile.nycschools.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobile.nycschools.MainViewModel
import com.mobile.nycschools.NYCSchoolsApplication
import com.mobile.nycschools.R
import com.mobile.nycschools.databinding.SchoolListFragmentBinding
import kotlinx.coroutines.launch

class SchoolListFragment : Fragment(), SchoolListAdapter.ListItemListener {

    private val viewModel: MainViewModel by activityViewModels()

    private lateinit var binding : SchoolListFragmentBinding
    private lateinit var adapter: SchoolListAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as NYCSchoolsApplication).appComponent
            .inject(viewModel)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        (activity as AppCompatActivity).supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(false)
        }
        binding = SchoolListFragmentBinding.inflate(inflater, container, false)

        with(binding.schoolListRecyclerView){
            setHasFixedSize(true)
            val divider = DividerItemDecoration(context, LinearLayoutManager(context).orientation)
            addItemDecoration(divider)
        }

        viewModel.schoolData.observe(viewLifecycleOwner){
            adapter = SchoolListAdapter(it, this@SchoolListFragment)
            with(binding.schoolListRecyclerView){
                adapter = this@SchoolListFragment.adapter
                layoutManager = LinearLayoutManager(activity)
            }
        }

        viewModel.isLoading.observe(this, Observer { isLoading ->
            binding.SchoolListProgressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        })

        viewModel.errorMessageSchoolData.observe(this, Observer { errorMessage ->
            if (errorMessage != null) {
                Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
            }
        })
        if(viewModel.schoolData.value == null){
            lifecycle.coroutineScope.launch {
                viewModel.fetchSchoolData()
            }

        }
        return binding.root
    }

    override fun onItemClick(position: Int) {
        viewModel.selectedSchoolPosition = position
        findNavController().navigate(
            R.id.action_schoolListFragment_to_schoolInformationFragment,
            null,
            navOptions {
                anim {
                    enter = android.R.animator.fade_in
                    exit = android.R.animator.fade_out
                }
            }
        )
    }

}