package com.mobile.nycschools.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.findNavController
import com.mobile.nycschools.MainViewModel
import com.mobile.nycschools.NYCSchoolsApplication
import com.mobile.nycschools.SAT_NOT_AVAILABLE
import com.mobile.nycschools.data.SchoolsSAT
import com.mobile.nycschools.databinding.SchoolInformationFragmentBinding
import kotlinx.coroutines.launch

class SchoolInformationFragment : Fragment() {
    // could have used databinding
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var binding:SchoolInformationFragmentBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as NYCSchoolsApplication).appComponent
            .inject(viewModel)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)
        binding = SchoolInformationFragmentBinding.inflate(inflater, container, false)

        // display already available information from school list service
        val schoolData = viewModel.schoolData.value?.get(viewModel.selectedSchoolPosition)
        schoolData?.let { data->
            with(binding){
                schoolName.text = data.school_name
                academicOppotunities.text = "${data.academicopportunities1}\n\n${data.academicopportunities2 ?: ""}"
                grade.text = data.finalgrades
                address.text = "${data.primary_address_line_1}\n${data.city}\n${data.school_email}\n${data.website}"
            }
        }

        viewModel.schoolSAT.observe(viewLifecycleOwner){
            val schoolSat:SchoolsSAT? = if(it.isNotEmpty()) it?.get(0) else null
            schoolSat?.let { data->
                with(binding){
                    avgSTAMaths.text = data.sat_math_avg_score
                    avgSTAReading.text = data.sat_critical_reading_avg_score
                    avgSTAWriting.text = data.sat_writing_avg_score
                }
            } ?: setDefaultSATValue(binding)
        }

        viewModel.isLoading.observe(this, Observer { isLoading ->
            binding.SchoolInformationProgressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        })

        viewModel.errorMessageSchoolSAT.observe(this, Observer { errorMessage ->
            if (errorMessage != null) {
                setDefaultSATValue(binding)
                Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
            }
        })

        lifecycle.coroutineScope.launch {
            viewModel.fetchSchoolSAT()
        }

        return binding.root
    }

    private fun setDefaultSATValue(binding: SchoolInformationFragmentBinding) {
        with(binding){
            avgSTAMaths.text = SAT_NOT_AVAILABLE
            avgSTAReading.text = SAT_NOT_AVAILABLE
            avgSTAWriting.text = SAT_NOT_AVAILABLE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this@SchoolInformationFragment.findNavController().navigateUp()
            else -> super.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun backPressAction(){
        requireActivity().onBackPressedDispatcher.addCallback(this, object :
            OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                this@SchoolInformationFragment.findNavController().navigateUp()
            }

        })
    }

}