package com.mobile.nycschools.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.nycschools.R
import com.mobile.nycschools.data.SchoolsData
import com.mobile.nycschools.databinding.ListItemBinding

class SchoolListAdapter(private val schoolList:List<SchoolsData>,
private val listener: ListItemListener
) :
RecyclerView.Adapter<SchoolListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val school = schoolList.get(position)
        with(holder.binding){
            schoolName.text = school.school_name
            schoolLocation.text =
                "${school.neighborhood}, ${school.city}"
            root.setOnClickListener {
                listener.onItemClick(position)
            }
        }
    }

    override fun getItemCount(): Int = schoolList.size

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView){
        val binding = ListItemBinding.bind(itemView)
    }

    interface ListItemListener {
        fun onItemClick(position:Int)
    }
}