package com.mobile.nycschools

const val WEB_SERVICE_BASE_URL = "https://data.cityofnewyork.us/resource/"
const val SCHOOL_LIST_URL = "s3k6-pzi2.json?\$limit=50"
const val SCHOOL_SAT_URL = "f9bf-2cp4.json"
const val ERROR_MSG = "Something went wrong, try again"
const val SAT_NOT_AVAILABLE = "Not Available"
