package com.mobile.nycschools.di

import com.mobile.nycschools.MainViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(viewModel: MainViewModel)
}