package com.mobile.nycschools.di

import com.mobile.nycschools.WEB_SERVICE_BASE_URL
import com.mobile.nycschools.api.SchoolsApi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun retrofitService(): SchoolsApi {
        return Retrofit.Builder()
            .baseUrl(WEB_SERVICE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolsApi::class.java)
    }

    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO
}