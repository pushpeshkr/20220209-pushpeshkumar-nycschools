package com.mobile.nycschools.data

data class SchoolsSAT(
    val dbn:String,
    val sat_math_avg_score:String,
    val sat_critical_reading_avg_score:String,
    val sat_writing_avg_score:String,
)