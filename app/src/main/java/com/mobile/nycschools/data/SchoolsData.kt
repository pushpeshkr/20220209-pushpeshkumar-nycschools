package com.mobile.nycschools.data

data class SchoolsData(
    val dbn:String,
    val school_name:String,
    val academicopportunities1:String,
    val academicopportunities2:String,
    val neighborhood:String,
    val primary_address_line_1:String,
    val phone_number:String,
    val school_email:String,
    val website:String,
    val finalgrades:String,
    val city:String
)