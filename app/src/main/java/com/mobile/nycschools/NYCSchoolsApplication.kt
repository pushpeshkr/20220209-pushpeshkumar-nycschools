package com.mobile.nycschools

import android.app.Application
import com.mobile.nycschools.di.DaggerAppComponent

class NYCSchoolsApplication : Application(){

    val appComponent by lazy {
        DaggerAppComponent.create()
    }

    override fun onCreate() {
        super.onCreate()
    }

}