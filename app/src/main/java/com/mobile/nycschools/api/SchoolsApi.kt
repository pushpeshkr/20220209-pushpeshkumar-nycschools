package com.mobile.nycschools.api
import com.mobile.nycschools.SCHOOL_LIST_URL
import com.mobile.nycschools.SCHOOL_SAT_URL
import com.mobile.nycschools.data.SchoolsData
import com.mobile.nycschools.data.SchoolsSAT
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {

    @GET(value= SCHOOL_LIST_URL)
    suspend fun getSchoolsData() : List<SchoolsData>

    @GET(value= SCHOOL_SAT_URL)
    suspend fun getSchoolsSAT(@Query("dbn") dbn: String) : List<SchoolsSAT>
}