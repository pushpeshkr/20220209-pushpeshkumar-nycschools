package com.mobile.nycschools.api

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WebServiceRepository @Inject constructor(
    private val schoolAPIService : SchoolsApi
) {
    fun retrofitService():SchoolsApi{
        return schoolAPIService
    }
}